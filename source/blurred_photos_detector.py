"""
    Automatically detects blurred photos in your library
"""

from utils.utils import *
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import scipy.ndimage as ndimage

def analyse(data):
    """

    :param data:
    :return:
    """
    # 2D fourier transform
    n = next_regular(max(data.shape[0:2]))
    ft = np.fft.fft2(data, s=(n, n), axes=(0, 1))
    ft = np.fft.fftshift(ft)

    # make a small cutout
    R = 600
    g = slice(int(n / 2 - R), int(n / 2 + R))
    ft = ft[g, g]

    # calculate energy density
    ft = ft.real ** 2 + ft.imag ** 2

    # addup the different color channels
    ft = np.sum(ft, axis=2)

    # normalization (not strictly necessary)
    ft = (ft - np.min(ft)) / (np.max(ft) - np.min(ft)) * 100

    # compute radial profile (truncate at R)
    ftr = radial_profile(ft, (R, R))[1]
    ftr = ftr[:R]
    r = np.arange(start=0, stop=R)

    # multiply by r
    ftr *= r

    # addup below and above T
    T = 150
    v1 = np.sum(ftr[r <= T])
    v2 = np.sum(ftr[r > T])
    v = v2 / v1

    return r, ftr, v

if __name__ == '__main__':

    # get this folder
    root_folder = os.path.realpath(os.path.dirname(__file__))

    text = read_text(os.path.join(root_folder, os.pardir, 'config_blurred.json'))
    config = json.loads(text)

    files = glob.glob(os.path.join(config['search path'], '**', '*.jpg'))
    # files = files[:50]
    number_files = len(files)

    print('Found {} photos'.format(number_files))
    ft_ratio = []
    for file in files:
        im = Image.open(file)
        print('Image {} with size {}, format {}, mode {}'.format(file, im.size, im.format, im.mode))

        # convert to numpy array will by dtype=uint8 and shape=(X,Y,3)
        data = np.array(im)

        v = analyse(data)[2]

        ft_ratio.append(v)

    # print histogram
    plt.hist(ft_ratio, label='FT ratio')
    plt.legend(loc='upper right')
    plt.show()

    # zip results and sort by ratio value
    results = zip(files, ft_ratio)
    results = sorted(results, key=lambda x: x[1])
    for result in results:
        print('file {} ratio {}'.format(*result))


