from utils.utils import *
import exifread

def filter_interesting_exif_tags(tags):
    """

    :param tags:
    :return:
    """
    keys = ('Image Model', 'Image DateTime', 'EXIF ExposureTime', 'EXIF FNumber')
    itags = (tags[x] for x in keys)
    return itags

def create_exif_database(root_path):
    """

    :param root_path:
    :return:
    """

    assert os.path.isdir(root_path)

    records = {}

    for dirpath, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            name = filename.lower()
            if name.endswith('.jpg') or name.endswith('.cr2'):
                file = os.path.join(dirpath, filename)

                # read exif tags
                with open(file, 'rb') as f:
                    tags = exifread.process_file(f)

                # store tags
                records[file] = tags

    # filter tags
    db = []
    for file, tags in records:
        itags = filter_interesting_exif_tags(tags)
        db.append((file, itags))