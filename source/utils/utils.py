"""
Useful helper routines.
"""

import glob
import os
import json
import numpy as np


def read_text(file):
    """
    Reads a whole text file (UTF-8 encoded).
    """
    with open(file, mode='r', encoding='utf-8') as f:
        text = f.read()
    return text


def write_text(file, text):
    """
    Writes a whole text file (UTF-8 encoded).
    """
    with open(file, mode='w', encoding='utf-8') as f:
        f.write(text)


def next_regular(target):
    """
    Find the next regular number greater than or equal to target.
    Regular numbers are composites of the prime factors 2, 3, and 5.
    Also known as 5-smooth numbers or Hamming numbers, these are the optimal
    size for inputs to FFTPACK.

    Target must be a positive integer.

    Taken from scipy
    """
    if target <= 6:
        return target

    # Quickly check if it's already a power of 2
    if not (target & (target-1)):
        return target

    match = float('inf')  # Anything found will be smaller
    p5 = 1
    while p5 < target:
        p35 = p5
        while p35 < target:
            # Ceiling integer division, avoiding conversion to float
            # (quotient = ceil(target / p35))
            quotient = -(-target // p35)

            # Quickly find next power of 2 >= quotient
            try:
                p2 = 2**((quotient - 1).bit_length())
            except AttributeError:
                # Fallback for Python <2.7
                p2 = 2**(len(bin(quotient - 1)) - 2)

            N = p2 * p35
            if N == target:
                return N
            elif N < match:
                match = N
            p35 *= 3
            if p35 == target:
                return p35
        if p35 < match:
            match = p35
        p5 *= 5
        if p5 == target:
            return p5
    if p5 < match:
        match = p5
    return match


def radial_profile(data, center):
    y, x = np.indices(data.shape)
    r = np.sqrt((x - center[0])**2 + (y - center[1])**2)
    r = r.astype(np.int)
    """
    Given a 2D data set and the coordinates of a center, calculates the radius and the radial profile.

    tbin = np.bincount(r.ravel(), data.ravel())
    nr = np.bincount(r.ravel())
    radialprofile = tbin / nr
    return radialprofile
    The radial profile is a average value of all the data values with the same distance from the center

    :param data:
    :param center:
    :return: The number per bins and the radial profile (0 if the number in the bin is 0)
    """

    # calculate radius and round down to integers
    grid = np.indices(data.shape)
    radius = np.sqrt((grid[0] - center[0])**2 + (grid[1] - center[1])**2)
    radius = radius.astype(np.int)

    # binning of radius and data
    sum_in_bin = np.bincount(radius.ravel(), data.ravel())
    number_in_bin = np.bincount(radius.ravel())
    bin_not_empty = number_in_bin > 0

    # normalizing summed data by number of elements
    profile = np.zeros(sum_in_bin.shape)
    profile[bin_not_empty] = sum_in_bin[bin_not_empty] / number_in_bin[bin_not_empty]
    return number_in_bin, profile


def remove_jpg_in_jpg_cr2_pairs(root_path):
    """

    :param root_path:
    """

    assert os.path.isdir(root_path)

    for dirpath, dirnames, filenames in os.walk(root_path):

        jpgs = (x for x in filenames if x.lower().endswith('.jpg'))

        for filename in jpgs:
            jpg_file = os.path.join(dirpath, filename)
            cr2_file = jpg_file[:-4] + '.CR2'

            if os.path.isfile(cr2_file):
                print('remove {}'.format(jpg_file))
                os.remove(jpg_file)
