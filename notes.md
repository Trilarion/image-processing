Reading raw files

- rawpy (https://github.com/letmaik/rawpy)
- rawkit (https://github.com/photoshell/rawkit)
- exifread (https://github.com/ianare/exif-py) installs from pypi and works

Progress bar

https://github.com/tqdm/tqdm
