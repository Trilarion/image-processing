# image-processing

Collection of Python image processing routines

Goals:

- face detection
- hdr joint image composition
- panorama/stitching/alignment
- blurry image detection
- deconvolution/denoising (moon/star imaging)
- shake-reduction for video
- spot removal (dust particles)
- duplicate detection (even those which are only very similar)
- filter for nice effects (edges, ...)
- raw format import
- GPU acceleration
- PyQt based graphical user interface